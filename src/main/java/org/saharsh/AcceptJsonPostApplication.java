package org.saharsh;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Map;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class AcceptJsonPostApplication {

	private static final Logger LOG = System.getLogger(AcceptJsonPostApplication.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(AcceptJsonPostApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			// No-op for now given auto scan
		};
	}

	@PostMapping("/")
	public void index(@RequestBody String body, @RequestHeader Map<String, String> headers) {
		LOG.log(Level.INFO, String.join(": ", "Received headers:", headers.toString()));
		LOG.log(Level.INFO, String.join(": ", "Received body:", body));
	}

}
