# Accept JSON Post

Simple web service that will accept any JSON payload over a POST request, and log the request body to console. Built using Spring Boot.

## Build and run locally

```
mvn spring-boot:run
```

Service will run on `localhost:8080` by default.
